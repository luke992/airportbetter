
public class Bags {

    private Flight flight;
    private BoardingPass boardingPass;
    //private int weight;//

    public Bags(Flight flight) {
        this.flight = flight;
        this.boardingPass = null;
    }

    public String getInbound() {
        return this.flight.inBound;
    }

    public String getFlightNo() {
        return this.flight.flightNo;
    }

    //public int getWeight() {
      //  return this.weight;
    //}

    public void setBoardingPass(BoardingPass pass) {
        this.boardingPass = pass;
    }

    public BoardingPass getBoardingPass() {
        return boardingPass;
    }

    public int getRow() {
        return this.boardingPass == null ? null : this.boardingPass.row;
    }

    public int getSeat() {
        return this.boardingPass == null ? null : this.boardingPass.seat;
    }
}
