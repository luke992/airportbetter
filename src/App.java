public class App {
    public static void main(String[] args) throws Exception {
        Airport LHR = new Airport("LHR");
        Airport LAX = new Airport("LAX");
        //airport created
        Passenger passenger = new Passenger(new Flight("LHR", "LAX", "LA992"));
        Bags bag = new Bags(new Flight("LHR", "LAX", "LA992"));
        Passenger passenger2 = new Passenger(new Flight("LHR", "LAX", "LA992"));
        Bags bag2 = new Bags(new Flight("LHR", "LAX", "LA992"));
        //new bags and passengers created
        Plane plane = new Plane("LA992");
        //plane created
        LHR.land(plane);
        //assign/land plane at airport to LHR
        LHR.departures(passenger, bag);
        LHR.departures(passenger2, bag2);
        // method called to assign bPass to bags and passenger and board them on plane using LA992
        LHR.takeOff("LA992");
        //plane takes off for flight
        LAX.land(plane);
        //plane re lands at LAX
    }
}
