import java.util.Arrays;

public class Plane {
    private String flightNo;
    private Passenger[][] seats;
    private Bags [][] hull;
    private String inBoundCode;


    public Plane(String flightNo) {
        this.flightNo = flightNo;
        this.seats = new Passenger[][] {
            {null, null, null},
            {null, null, null},
            {null, null, null},
            {null, null, null},
            {null, null, null},
            {null, null, null}
        };
        this.hull = new Bags[][] {
            {null, null, null},
            {null, null, null},
            {null, null, null},
            {null, null, null},
            {null, null, null},
            {null, null, null}
        };
        this.inBoundCode =null;
    }

    public void board(Passenger passenger) {
        int row = passenger.getRow();
        int seat = passenger.getSeat();
        this.seats[row][seat] = passenger;
        this.inBoundCode = passenger.getInbound();
    }

    public void storeBags(Bags bags) {
        int row = bags.getRow();
        int seat = bags.getSeat();
        this.hull[row][seat] = bags;
        this.inBoundCode = bags.getInbound();
    }

    public String getFlightNo() {
        return this.flightNo;
    }

    public Boolean hasPassenger(Passenger passenger) {
        Boolean hasPassenger = false;
        for (Passenger[] row : this.seats) {
            hasPassenger = Arrays.asList(row).contains(passenger);
            if (hasPassenger) break;
        }
        return hasPassenger;
    }

    public int[] getNextFreeSeat() {
        int[] rowSeat = {0,0};
        foundRow:
        for(int row = 0;row < this.seats.length; row++) {
            for(int seat = 0; seat < this.seats[row].length; seat++) {
                if (this.seats[row][seat] == null) {
                    rowSeat[0] = row;
                    rowSeat[1] = seat;
                    break foundRow;
                }
            }
        }
        return rowSeat;
    }
    public String getInBound() {
        return this.inBoundCode;
    }
}
