import java.util.HashMap;

public class Airport {
    public static HashMap<String, Airport> all = new HashMap<>();

    private String code;
    private HashMap<String, Plane> planes;

    public Airport(String code) {
        this.code = code;
        this.planes = new HashMap<>();
        Airport.all.put(code,this);
    }

    public Plane getFlight(String flightNo) {
        return this.planes.get(flightNo);
    }

    public void departures(Passenger passenger, Bags bag) {
        Plane plane = this.planes.get(passenger.getFlightNo());
        int[] rowAndSeat = plane.getNextFreeSeat();
        BoardingPass bPass = new BoardingPass(rowAndSeat[0], rowAndSeat[1]);
        passenger.setBoardingPass(bPass);
        bag.setBoardingPass(bPass);
        plane.board(passenger);
        plane.storeBags(bag);
    }

    public void land(Plane plane) {
        this.planes.put(plane.getFlightNo(), plane);
    }

    public void takeOff(String flightNo) {
        //get the plane from this airport 
        Plane plane = this.planes.get(flightNo);
        //figure out where the plane is going 
        String inBound = plane.getInBound();
        //remove plane from this.plans
        this.planes.remove(flightNo);
        //land the plane at destination airport
        Airport destination = Airport.all.get(inBound);
        destination.land(plane);


    }
}
